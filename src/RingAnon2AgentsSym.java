import davis.core.idscheme.IdScheme;
import davis.core.idscheme.OrderedEnumeration;
import davis.core.network.AgentGenerator;
import davis.core.network.RingGenerator;
import io.jbotsim.core.Link;
import io.jbotsim.core.Node;
import io.jbotsim.core.Topology;

import java.util.ArrayList;
import java.util.List;

import davis.core.model.Agent;
import davis.core.model.Place;
import davis.core.model.RingAgent;
import davis.core.model.RingPlace;
import davis.core.model.StatefulNode;

public class RingAnon2AgentsSym<B> extends AgentGenerator<B> {

  private final int NUMBER_OF_AGENTS = 2;

  @Override
  public void generate(Topology topo, IdScheme id) {
    if (getNetworkSize() == 0) {
      System.out.println("Empty ?");
      return;
    }

    topo.setNodeModel("place", new RingPlace<B>().getClass());
    RingGenerator<RingPlace> ring = new RingGenerator<>();
    ring.defaultModel = "place";
    ring.setNetworkSize(getNetworkSize());
    ring.generate(topo, new OrderedEnumeration());

    // resetting runnins count
    StatefulNode.resetRunnings();
    List<RingAgent<B>> agents = new ArrayList<>();
    // 3 agents

    for (int agentCount = 0; agentCount < NUMBER_OF_AGENTS; agentCount++) {
      agents.add((RingAgent<B>) topo.newInstanceOfModel("default"));
    }

    int[] ids = {2, 3};
    int i = 0;
    for (RingAgent<B> a : agents) {
      a.setID(ids[i]);
      a.disableWireless();
      i++;
    }

    List<Node> nodes = topo.getNodes();
    List<Place<B>> places = new ArrayList<>();
    for (Node n : nodes) {
      places.add((Place<B>) n);
    }

    // add loops
    for (Place<B> p : places) {
      topo.addLink(new Link(p, p));
    }
    System.out.println("TOTO");
    generate(topo, places, agents);
  }

  @Override
  public void generate(
      Topology topo, List<? extends Place<B>> places, List<? extends Agent<B>> agents) {

    int location = 0;
    int step = this.getNetworkSize() / NUMBER_OF_AGENTS;

    for (Agent<B> a : agents) {
      Place<B> p = places.get(location);
      p.addAgent(a); // add to homebase Place
      a.place = p; // add place to agent
      a.setLocation(p.getLocation()); // location of agent is homebase
      a.translate(10, 10); // + epsilon
      System.out.println("GEN ON " + a.place);
      topo.addNode(a); // add agent to topology

      location += step;
    }
  }

  @Override
  public void generate(Topology topo, List<Place<B>> places) {
    //
  }
}
