import davis.core.idscheme.IdScheme;
import davis.core.model.RectangularAgent;
import davis.core.model.RectangularPlace;
import davis.core.network.AgentFactory;
import davis.core.network.RectangularGridGenerator;
import io.jbotsim.core.Topology;

public class RectangularTwoAgents<B> extends RectangularGridGenerator<RectangularPlace<B>> {

  @Override
  public void generate(Topology topo, IdScheme id) {
    Class defaultNode = topo.getDefaultNodeModel();
    topo.setDefaultNodeModel(RectangularPlace.class);
    super.generate(topo, id);
    topo.setDefaultNodeModel(defaultNode);
    AgentFactory.<RectangularAgent<B>, B>nAgentsAtRandomPlace(topo, 2);
  }
}
