import davis.core.agents.rendez_vous.RDVRectangularAgent;
import davis.core.model.RectangularAgent;

import java.util.ArrayList;
import java.util.Random;

public class RandomWalkPath extends RDVRectangularAgent {
  Random random;

  public void init() {
    this.random = new Random();
    this.become("randomWalk");
  }

  public void randomWalk(Object Board) {
    if (random.nextBoolean()) {
      if (!goRight()) goLeft();
    } else {
      if (!goLeft()) goRight();
    }
  }
}
