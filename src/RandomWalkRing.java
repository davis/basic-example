import davis.core.agents.rendez_vous.RDVRingAgent;

import java.util.Random;

public class RandomWalkRing extends RDVRingAgent {
  Random random;

  public void init() {
    this.random = new Random();
    this.become("randomWalk");
  }

  public void randomWalk(Object board) {
    if (random.nextBoolean()) {
      goLeft();
    } else if (random.nextBoolean()) {
      goRight();
    } else stay();
  }
}
