# Basic example of using Davis 

## Install dependencies locally

```
./gradlew downloadAndUnzipFile
```

## Getting started without IDE

1. Write your java algorithm into the `src` folder : `MyAlgo.java`
2. Configure your `PATH` variable
```
export PATH=$PATH:`pwd`/build/install/davis-beta/bin
```
3. Execute in the `src` directory
```
udavis -n <networkGeneratorFile> -i <ids> -s <size> -al <algoFile : MyAlgo.java>
```


## Working with `gradle`

1. Modify `commandLine` parameters for task `davis` in `build.gradle`.
3. Run
```
./gradlew hello
```


You can also collect all your examples in a `scenario` file

1. Open `scr/scenario` file and add a line with 5 parameters :
`[<scenarioName>] -n <networkGeneratorFile> -i <scheme> -s <size> -al <algoFile : MyAlgo.java>`
3. Execute in a terminal with `./gradlew run[scenarioName]`


## Getting started with Intellij

 Open IntelliJ and load project :
```
File/Open
```

Choose the `basic-example` folder and click ok for *auto-import*.


## Working with Intellij and `gradle`

1. Write your java algorithm into the `src` folder : `MyAlgo.java`
2. Open `src/scenario` file and add a line with 5 parameters :
`[<scenarioName>] -n <networkGeneratorFile> -i <scheme> -s <size> -al <algoFile : MyAlgo.java>`
3. Refresh gradle panel.
4. In `Tasks/application` of the gradle panel you should see your scenario as a 
gradle task named `run[<scenarioName>]`.
5. Click to execute.



## Docs

See also the [documentation for davis](https://gitlab.lis-lab.fr/davis/davis/tree/master/doc).






